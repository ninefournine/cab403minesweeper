#CAB403 Minesweeper

## Compile instructions

`make` in both the client and server folders

## Running:

Run the server first as:

`./out <PORT>`

Then the client as:

`./out <SERVER_IP> <PORT>`