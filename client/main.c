#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include "../common/packets.h"
#include "../common/constants.h"

/* State machine for the menus. */
enum MenuState {
    MENU_MAIN,
    MENU_INGAME,
    MENU_GAME_OVER,
    MENU_GAME_OVER_WIN
};

enum MenuState current_menu = MENU_MAIN;

// Stores the characters of the mines.
char board[9][9];
uint8_t remaining_mines;


// TODO: implement properly
void wait_for_enter() {
    printf("\nPRESS ANY CHARACTER KEY FOLLOWED BY [ENTER] TO CONTINUE...\n");
    char t;
    while (scanf(" %c", &t) != 1) continue;
}

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

/*
Handle authentication. Returns 0 if our authentication details are wrong.
*/
int do_authentication(int fd) {
    // Get user input.
    char username[MAX_USERNAME_LENGTH];
    char password[MAX_PASSWORD_LENGTH];
    printf("===========================\n"
           "A U T H E N T I C A T I O N\n"
           "===========================\n");
    printf("Username:\t");
    while (scanf("%s", username) != 1) continue;     // Loop until valid input.
    printf("Password:\t");
    while (scanf("%s", password) != 1) continue;

    // Send it to the server.
    packet_send(fd, "ss", username, password);

    // Get back the response from the server.
    int rc;
    uint8_t authenticated;
    rc = packet_recv(fd, "c", &authenticated);
    if (rc == -1) {
        // The server disconnected (or our internet went down) if packet_recv() returned -1
        printf("Server was unable to receive authentication details. Disconnecting...\n");
        return 0;
    }
    if (!authenticated) {
        printf("Invalid credentials! Disconnecting...\n");
        return 0; // the server also disconnects on the other side
                  // so there isn't any potential "hacked client" issue here
    }
    return 1;
}

void clear_board() {
    for (int i = 0; i < NUM_TILES_X*NUM_TILES_Y; i++) {
        *(board[0]+i) = '.';
    }
}

/*
Show the board. On the client side it's just an array of chars, so like this:
*/
void showboard() {
    printf("Remaining Mines: %d\n\n", remaining_mines);
    printf("\n");
    for (int k = 0; k < NUM_TILES_X; k++){
        printf("\t%d", k + 1);
    }
    printf("\n\t_________________________________________________________________\n");
    for (int i = 0; i < NUM_TILES_X; i++){
        printf("%d|\t", i + 1);
        for (int j = 0; j < NUM_TILES_Y; j++){
            printf("%c", board[i][j]);
            if (j == NUM_TILES_X - 1){
                printf("\n");
            }
            else {
                printf("\t");
            }
        }
    }
}

/*
Get coordinates and write them out. Checks for valid input too.
*/
void get_coordinates(int* row, int* column) {
    printf("Please enter your row: (1-9)");
    while (scanf("%d", row) != 1 || *row > 9 || *row < 1) continue;
    printf("\nPlease enter your column: (1-9)");
    while (scanf("%d", column) != 1 || *column > 9 || *column < 1) continue;
}

/*
Starts the game.
*/
int start_game(int fd, char selection, enum MenuState* current_menu) {
    // let the server know we want to start a new game
    packet_send(fd, "c", selection);
    // The server replies with how many mines there are.
    if (packet_recv(fd, "c", &remaining_mines) == -1) {
        printf("\n\n\nUnfortunately the server has disconnected. Good-bye.\n\n\n\n");
        return 0;   // the server's disconnected
    }
    clear_board();
    *current_menu = MENU_INGAME;
    return 1;
}

/*
Reveals a tile.
*/
int reveal_tile(int fd, char selection, enum MenuState* current_menu) {
    // Let the server know we're going to try and reveal a tile.
    packet_send(fd, "c", selection);

    int row, column;
    get_coordinates(&row, &column);
    // Send the server the coordinates now.
    packet_send(fd, "cc", (uint8_t)row, (uint8_t)column);

    // Receive the response from the server, it's pretty big
    uint8_t response_code, no_tiles;
    uint8_t x_coords[TOTAL_TILES];
    uint8_t y_coords[TOTAL_TILES];
    char characters[TOTAL_TILES];

    // this means: receive two uint8_ts, then three arrays of uint8_ts
    if (packet_recv(fd, "cc#c#c#c",
        &response_code,
        &remaining_mines,
        &no_tiles,
        x_coords,
        &no_tiles,
        y_coords,
        &no_tiles,
        characters) == -1) {
        printf("oh oh\n");
        return 0;
    }
    printf("Number of tiles revealed: %d\n", no_tiles);

    // Set the tiles revealed. note client goes 0-8 and server goes 1-9
    for (int i = 0; i < no_tiles; i++) {
        board[y_coords[i]-1][x_coords[i]-1] = characters[i];
    }

    // Check the response codes and act accordingly
    if (response_code == RESP_GAME_OVER) {
        *current_menu = MENU_GAME_OVER;
    }

    if (response_code == RESP_ALREADY_REVEALED) {
        printf("TIle already revealed!\n");
    }

    return 1;
}

/*
Place a flag.
*/
int place_flag(int fd, char selection, enum MenuState* current_menu) {
  // let the server know we're going to place a flag.
  packet_send(fd, "c", selection);

  // Send the server the coordinates now.
  int row, column;
  get_coordinates(&row, &column);
  packet_send(fd, "cc", (uint8_t)row, (uint8_t)column);

  // Receive the response from the server.
  uint8_t response_code, remaining_mines_after_flag;
  packet_recv(fd, "cc", &response_code, &remaining_mines_after_flag);

  // Check the response code
  if (response_code == RESP_ALREADY_REVEALED){
    printf("Tile already revealed\n");
  }
  else if (response_code == RESP_OUT_OF_BOUNDS){
    printf("Coordinates out of bounds\n");
  }

  // We don't have a response code for this, just check the mine count instead
  if (remaining_mines == remaining_mines_after_flag){
    printf("That was not a mine.\n");
  }
  else {
    // we flagged a mine!
    remaining_mines = remaining_mines_after_flag;
    if (remaining_mines == 0){ // victory condition
      // the server will now send us the whole board and our time
      uint16_t array_length;
      char data[TOTAL_TILES];
      uint16_t time_taken;
      packet_recv(fd, "#ch", &array_length, data, &time_taken);

      // The server sends us the data in the order of memory, so we're allowed
      // to do this instead of a double for loop for X and Y
      for (int i = 0; i < TOTAL_TILES; i++){
        *(board[0]+i) = data[i];
      }


      printf("\n\nGame Length: %d seconds\n\n", time_taken);
      *current_menu = MENU_GAME_OVER_WIN;
    }
    // We haven't won yet but we can place the flag now
    board[row-1][column-1] = 'P';
  }

  return 1;
}

/*
Shows the leaderboard.
*/
int show_leaderboard(int fd, char selection) {
    // Let the server know we want the leaderboard.
    packet_send(fd, "c", selection);
    // The server tells us if there are records or not.
    uint8_t are_there_records;
    if (packet_recv(fd, "c", &are_there_records) == -1) {
        printf("\n\n\nUnfortunately the server has disconnected. Good-bye.\n\n\n\n");
        return 0;
    }
    if (are_there_records == 0) { // There are no records.
        printf("\n\nThere are no records in the leaderboard at the moment.\n\n\n");
        return 1;
    }
    // There are records. The server keeps sending us them until it's done
    printf("User\tTime\t\tWins\tGames Played\n");
    uint8_t more_records_to_recv = 1;   // 0 on the last record
    while (more_records_to_recv) {
        char username[32];
        uint16_t seconds;
        uint16_t games_won;
        uint16_t games_played;
        if (packet_recv(fd, "cshhh", &more_records_to_recv, username, &seconds, &games_won, &games_played) == -1) {
            more_records_to_recv = 0;
        }
        printf("%s\t%d seconds\t%d won\t%d played\n", username, seconds, games_won, games_played);
    }
    return 1;
}

/*
The main loop of the program. Return 0 to exit.
*/
int process(int fd) {
    if (current_menu == MENU_MAIN) {
        printf("\n\n\n\nWelcome to the Minesweeper Gaming System.\n"
               "Please enter a selection:\n"
               "<1>\t Play Minesweeper\n"
               "<2>\t Show Leaderboard\n"
               "<3>\t Quit\n\n");
        printf("Please enter a selection:\n");
        char selection;
        while (scanf(" %c", &selection) != 1) continue;

        // //Debugging code
        // printf("you made a selection!\n");

        switch (selection) {
            case '1':
                return start_game(fd, selection, &current_menu);
            case '2':
                return show_leaderboard(fd, selection);
                break;
            case '3':
                // quit
                printf("\n\nYou have elected to quit. Good-bye.\n\n");
                return 0;
            default:
                printf("Invalid selection!\n");
                break;
        }
        return 1;
    }
    if (current_menu == MENU_INGAME) {
        showboard();
        printf("\nChoose an option:\n");
        printf("<R> Reveal Tile\n");
        printf("<P> Place Flag\n");
        printf("<Q> Quit Game\n\n");

        printf("Option: (R,P,Q): ");

        char selection;
        while (scanf(" %c", &selection) != 1) continue;
        switch (selection) {
            case 'R':
            case 'r':
                return reveal_tile(fd, selection, &current_menu);
            case 'P':
            case 'p':
                return place_flag(fd, selection, &current_menu);
            case 'Q':
            case 'q':
                printf("\n\n\nYou have quit the current game.\n\n\n\n");
                current_menu = MENU_MAIN;
                return 1;
            default:
                printf("Invalid selection!");
                break;
        }
    }
    if (current_menu == MENU_GAME_OVER) {
        printf("\n\n======================\n"
               "G A M E        O V E R\n"
               "======================\n\n\n");
        showboard();
        wait_for_enter();
        current_menu = MENU_MAIN;
    }
    if (current_menu == MENU_GAME_OVER_WIN) {
      printf("\n\n======================\n"
             "Y O U            W I N\n"
             "======================\n\n\n");
      showboard();
      wait_for_enter();
      current_menu = MENU_MAIN;
    }
    return 1;
}

int main(int argc, char *argv[])
{
    int sockfd;
    struct addrinfo hints, *servinfo, *p;
    int rv;
    char s[INET6_ADDRSTRLEN];

    if (argc != 3) {
        fprintf(stderr,"usage: client hostname port\n");
        exit(1);
    }

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    if ((rv = getaddrinfo(argv[1], argv[2], &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }

    // loop through all the results and connect to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                p->ai_protocol)) == -1) {
            perror("client: socket");
            continue;
        }

        if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("client: connect");
            continue;
        }

        break;
    }

    if (p == NULL) {
        fprintf(stderr, "client: failed to connect\n");
        return 2;
    }

    inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
            s, sizeof s);
    printf("client: connecting to %s\n", s);

    freeaddrinfo(servinfo); // all done with this structure


    if (do_authentication(sockfd)) {
    	while (process(sockfd)) {
            // Loop until process() returns 0.
            //Debugging Code
            // printf("===END OF LOOP===\n");
    	}
    }

    close(sockfd);

    return 0;
}
