/*
Constants that should be the same on the client and the server.
*/

#define MAX_USERNAME_LENGTH 30
#define MAX_PASSWORD_LENGTH 30

#define NUM_TILES_X 9
#define NUM_TILES_Y 9
#define TOTAL_TILES 81

#define RESP_ALL_GOOD 0
#define RESP_GAME_OVER 1
#define RESP_ALREADY_REVEALED 2
#define RESP_OUT_OF_BOUNDS 3
#define RESP_YOU_WIN 4
