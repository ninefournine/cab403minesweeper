#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "packets.h"


void add_uint8_t(char* buf, int* offset, uint8_t value) {
	// It's a single byte, so no network byte order needed.
	*(buf+*offset) = value;
	(*offset)++;
}

void get_uint8_t(char* buf, int* offset, uint8_t* value) {
	*value = *(buf+*offset);
	(*offset)++;
}

void add_uint16_t(char* buf, int* offset, uint16_t value) {
	// convert to network byte order first.
	uint16_t network_value = htons(value);
	memcpy(buf+*offset, &network_value, sizeof(uint16_t));
	(*offset) += sizeof(uint16_t);
}

void get_uint16_t(char* buf, int* offset, uint16_t* value) {
	uint16_t network_value = 0;
	memcpy(&network_value, buf+*offset, sizeof(uint16_t));
	*value = ntohs(network_value);
	(*offset) += sizeof(uint16_t);
}

void add_string(char* buf, int* offset, char* value) {
	int len = strlen(value); // len doesn't include null terminator
	strcpy(buf+*offset, value); // but strcpy does copy the \0 over, which means...
	(*offset) += len + 1; // the offset increases by len and the \0.
}

void get_string(char* buf, int* offset, char* value) {
	int len = strlen(buf+*offset);
	strcpy(value, buf+*offset);
	(*offset) += len + 1;
}

/*
Pack binary items into a buffer. Wrapper for vpack()
*/
int pack(char* buf, const char* format, ...) {
	va_list args;
	va_start(args, format);
	int return_value = vpack(buf, format, args);
	va_end(args);
	return return_value;
}

int vpack(char* buf, const char* format, va_list args) {
	int offset = 0;
	const char* p = format;
	uint16_t array_length = 0;
	int array_index = 0;

	// array arguments.
	uint8_t* c_array;
	uint16_t* h_array;

	while (*p != '\0') {
		if (*p == 'c') {
			uint8_t value;
			if (array_length) { // Get value from array.
				// Only get the array argument the first time.
				c_array = (array_index == 0) ? va_arg(args, uint8_t*) : c_array;
				value = c_array[array_index];
			} else { // Get value directly from the arguments instead.
				value = (uint8_t)va_arg(args, int);
			}
			add_uint8_t(buf, &offset, value);
		}
		if (*p == 'h') {
			uint16_t value;
			if (array_length) {
				h_array = (array_index == 0) ? va_arg(args, uint16_t*) : h_array;
				value = h_array[array_index];
			} else {
				value = (uint16_t)va_arg(args, int);
			}
			add_uint16_t(buf, &offset, value);
		}
		if (*p == 's') { // arrays of strings (char**) not supported. use #c instead
			char* value;
		 	value = va_arg(args, char*);
			add_string(buf, &offset, value);
		}
		if (*p == '#') {
			// read the length.
			uint16_t length = (uint16_t)va_arg(args, int);
			array_length = length;
			// write the length to the buffer.
			add_uint16_t(buf, &offset, length);
			array_index = 0;
			// Increment the pointer (to the type of the array)
			p++;
			continue; // skip the first array_index check
		}
		// Have we reached the end of the array? Then we can move on
		if (array_index == array_length - 1 || array_length == 0) {
			array_index = 0;
			array_length = 0;
			p++;
		} else {
			array_index++;
		}
	}
	va_end(args);
	return offset;
}

/*
Pack binary items into a buffer. Wrapper for vunpack()
*/
int unpack(char* buf, const char* format, ...) {
	va_list args;
	va_start(args, format);
	int return_value = vunpack(buf, format, args);
	va_end(args);
	return return_value;
}

/*
Unpack from a buffer into items.
*/
#ifndef __clang__
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif
int vunpack(char* buf, const char* format, va_list args) {
	int offset = 0;
	const char* p = format;
	uint16_t array_length = 0;
	int array_index = 0;

	// "normal" and "array" arguments are both the same type for unpacking
	uint8_t* c_values;
	uint16_t* h_values;
	char* s_values;

	while (*p != '\0') {
		if (*p == 'c') {
			// we get a new argument if it's a new array, or an individual item
			if (array_index == 0 || array_length == 0) {
				c_values = va_arg(args, uint8_t*);
			}
			get_uint8_t(buf, &offset, c_values+array_index);
		}
		if (*p == 'h') {
			if (array_index == 0 || array_length == 0) {
				h_values = va_arg(args, uint16_t*);
			}
			get_uint16_t(buf, &offset, h_values+array_index);
		}
		if (*p == 's') {
			s_values = va_arg(args, char*);
			get_string(buf, &offset, s_values);
		}
		if (*p == '#') {
			// Get the length from the buffer.
			uint16_t* length = va_arg(args, uint16_t*);
			get_uint16_t(buf, &offset, length);
			array_length = *length;
			array_index = 0;
			// increment the pointer to the type of the array.
			p++;
			continue; // skip the first check
		}
		if (array_index == array_length - 1 || array_length == 0) {
			array_index = 0;
			array_length = 0;
			p++;
		} else {
			array_index++;
		}
	}
	return offset;
}
#ifndef __clang__
#pragma GCC diagnostic error "-Wmaybe-uninitialized"
#endif

int packet_send(int fd, const char* format, ...) {
	char buffer[OUTPUT_BUFFER_SIZE];
	va_list args;
	va_start(args, format);
	// Reserve two bytes at the start for the length.
	int length = vpack(buffer+sizeof(uint16_t), format, args);

	//Debugging code
	// printf("Length of packet: %d\n", length);

	int network_length = htons(length);
	// Now add the length to the packet
	memcpy(buffer, &network_length, sizeof(uint16_t));
	// length doesn't include the actual length itself, so the full length is
	int packet_length = length + sizeof(uint16_t);

	//Debugging code
	// printf("The packet we're sending:\n");
	// for (int i = 0; i < packet_length; i++) {
	// 	printf("%X ", buffer[i]);
	// }
	// printf("\n");

	// Potentially these packets could be pretty big! So we cannot assume that
	// send() will work in one go...
	int bytes_sent = 0;
	int rn = 0;
	while (bytes_sent < packet_length) {
		rn = send(fd, buffer+bytes_sent, (packet_length-bytes_sent), 0);
		if (rn == -1) { // disconnect
			break;
		}
		bytes_sent += rn;
	}
	va_end(args);

	return rn == -1 ? -1 : 0; // return -1 on failure, 0 on success
}

int packet_recv(int fd, const char* format, ...) {
	char buffer[INPUT_BUFFER_SIZE];
	va_list args;
	va_start(args, format);
	// Read the two bytes at the start to get the length first...
	uint16_t packet_length;
	int rc = recv(fd, &packet_length, sizeof(uint16_t), 0);
	packet_length = ntohs(packet_length);
	// Check if they want us to receive too much for our input buffer
	// Our client will never send too much, so if this happens it's a malicious client
	// and we'll just terminate the connection straight away
	if (rc == 0 || rc == -1 || packet_length > INPUT_BUFFER_SIZE) {
		return -1;	// disconnect!
	}
	// Then receive the rest of the packet. Again, recv() might not work in one go.
	int bytes_received = 0;
	int rn = 0;
	while (bytes_received < packet_length) {
		rn = recv(fd, buffer+bytes_received, (packet_length-bytes_received), 0);
		if (rn == 0 || rn == -1) { // disconnect
			printf("oof\n");
			return -1;
		}
		bytes_received += rn;
	}

	//Debugging Code
	// printf("The packet we got:\n");
	// for (int i = 0; i < packet_length; i++) {
	// 	printf("%X ", buffer[i]);
	// }
	// printf("\n");
	vunpack(buffer, format, args);
	return 0;
}
