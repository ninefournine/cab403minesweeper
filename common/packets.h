/*
packets.h and packets.c

Handle communication between the client and the server. All communications
is done through here.

Data is serialised into packets. Packets are defined via 'format strings' as follows:

c - char (uint8_t or int8_t)
h - short (uint16_t or int16_t)
s - string
# - an array (length specified with a uint16_t)
 (e.g. an array of shorts is #h)

EXAMPLE: I want to send two chars, an array of shorts, then a string.
Sender:
packet_send(fd, "cc#hs", 2, 3, arr, "abcdefg");
Receiver:
packet_recv(fd, "cc#hs", &out1, &out2, out3, out4);

The most important thing with this strategy is the client and server must always
"be on the same page" with what they intend to send and receive -- if the client
calls packet_recv with a different format string to the server's packet_send, it
usually ends badly.
*/

#ifndef PACKETS_H
#define PACKETS_H

#include <stdint.h>
#include <stdarg.h>
#include "constants.h"

#define INPUT_BUFFER_SIZE 512
#define OUTPUT_BUFFER_SIZE 512

/*
Response structure used for when the server wants to reveal some tiles to the client.
*/
struct Response
{
    uint8_t response_code;  // see constants.h
    uint8_t number_of_mines;
    uint8_t number_of_tiles;
    uint8_t x_coordinates[TOTAL_TILES];
    uint8_t y_coordinates[TOTAL_TILES];
    char characters[TOTAL_TILES];
};

/*
Add a uint8_t/uint16_t/null-terminated string to a buffer at
the specified offset. Converts to network byte order when needed.
*/
void add_uint8_t(char* buf, int* offset, uint8_t value);
void add_uint16_t(char* buf, int* offset, uint16_t value);
void add_string(char* buf, int* offset, char* value);

/*
Parses out a uint8_t/uint16_t/null-terminated string from a buffer at
the specified offset. Converts back from network byte order when needed.
*/
void get_uint8_t(char* buf, int* offset, uint8_t* value);
void get_uint16_t(char* buf, int* offset, uint16_t* value);
void get_string(char* buf, int* offset, char* value);

/*
Pack arguments defined by a format string into a buffer.
*/
int pack(char* buf, const char* format, ...);
int vpack(char* buf, const char* format, va_list args);

/*
Unpack arguments from a buffer into arguments, defined by a format string.
*/
int unpack(char* buf, const char* format, ...);
int vunpack(char* buf, const char* format, va_list args);

/* Send arguments to a receiver defined by a format string. */
int packet_send(int fd, const char* format, ...);

/* Recieve data and unpack to arguments, defined by a format string. */
int packet_recv(int fd, const char* format, ...);

#endif