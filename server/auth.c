#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "auth.h"

/*
Returns the user ID if the authentication is correct,
otherwise return -1.
*/
int authenticate(char* username, char* password) {
	int count = 0;
	for (struct user* current = HEAD; current != NULL; current = current->next) {
		if (strcmp(current->username, username) == 0) {
			if (strcmp(current->password, password) == 0) {
				return count;
			} else {
				return -1;
			}
		}
		count++;
	}
	return -1;
}

/*
Given a user ID, returns the username
*/
char* get_username_from_id(int id) {
	struct user* current = HEAD;
	for (int i = 0; i < id; i++) {
		current = current->next;
	}
	return current->username;
}

/*
Add a user to the authentication system.
*/
void add_user(char* username, char* password) {
	struct user* new = (struct user*)malloc(sizeof(struct user));
	new->username = strdup(username);
	new->password = strdup(password);
	new->next = NULL;
	// No items in the list yet.
	if (HEAD == NULL) {
		HEAD = new;
		TAIL = new;
	} else {
		TAIL->next = new;
		TAIL = new;
	}
}

/* Load authentication data from a file. Returns the number of users added */
int load_authentication_data(char* filename) {
	int count = 0;
	authentication_file = fopen(filename, "r");
	if (authentication_file == NULL) {
		fprintf(stderr, "Can't open authentication file %s!\n", filename);
		exit(1);
	}
	char username[32];
	char password[32];
	while (fscanf(authentication_file, "%s %s", username, password) != EOF) {
		//printf("%s %s\n", username, password);
  		add_user(username, password);
		count++;
	}
	// sigint?
	fclose(authentication_file);
	return count;
}

/*
Frees the data associated with the authentication data structure.
*/
void free_authentication_data() {
	struct user* tmp;
	while (HEAD != NULL) {
		tmp = HEAD;
		HEAD = HEAD->next;
		free(tmp->username);
		free(tmp->password);
		free(tmp);
	}
}


