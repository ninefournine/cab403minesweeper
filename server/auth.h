/*
Auth.h

Provides functions to add users, and check usernames and passwords.
Internally, the data is stored as a singularly-linked list.
*/

#ifndef AUTH_H
#define AUTH_H

struct user {
	char* username;
	char* password;
	struct user* next;
};

struct user* HEAD;
struct user* TAIL;

FILE* authentication_file;

int authenticate(char* username, char* password);
void add_user(char* username, char* password);
int load_authentication_data(char* filename);
void free_authentication_data(void);
char* get_username_from_id(int id);

#endif