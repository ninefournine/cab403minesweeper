/*
high_scores.c
*/
#include "high_scores.h"
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "readerwriter.h"

/*
Add a record to the high score system.
*/
void add_record(int user_id, int finish_time) {
	struct Record* new_record = (struct Record*)malloc(sizeof(struct Record));
	new_record->user_id = user_id;
	new_record->seconds = finish_time;

	new_record->lchild = NULL;
	new_record->rchild = NULL;

	// Special case: no items yet.
	if (ROOT == NULL) {
		ROOT = new_record;
	} else {
		struct Record* current = ROOT;
		bool found = false;
		while (!found) {
			if (finish_time < (current->seconds)) {
				// Insert into left tree.
				if (current->lchild == NULL) {
					current->lchild = new_record;
					found = true;
				} else {
					current = current->lchild;
				}
			} else {
				// Insert into right tree.
				if (current->rchild == NULL) {
					current->rchild = new_record;
					found = true;
				} else {
					current = current->rchild;
				}
			}
		}
	}
}

/*
Initialise the data structure for storing win/loss statistics.
*/
void init_statistics(int len) {
	statistics = (struct WinStatistic*)malloc(sizeof(struct WinStatistic)*len);
	for (int i = 0; i < len; i++) {
		statistics[i].games_won = 0;
		statistics[i].games_played = 0;
	}
}

/*
Free the data structure for storing win/loss statistics.
*/
void free_statistics() {
	free(statistics);
}

/*
Free the data structure for storing records
*/
void free_records(struct Record* root) {
	if (root == NULL) {
		return;
	}
	free_records(root->lchild);
	free_records(root->rchild);
	free(root);
}

/*
Called when the player wins.
TODO: synchronisation
*/
void on_win(int user_id, int finish_time) {
	WriteLock();

	//Debugging Code
	// printf("To test the critical-solution problem writing to the scoreboard takes 10 seconds.\n");
	// for (int i = 10; i != 1; i--) {
	// 	printf("%d\n", i);
	// 	sleep(1);
	// }
	// printf("\n");

	statistics[user_id].games_won++;
	statistics[user_id].games_played++;
	add_record(user_id, finish_time);
	WriteUnlock();
}

/*
Called when the player loses.
*/
void on_loss(int user_id) {
	WriteLock();
	statistics[user_id].games_played++;
	WriteUnlock();
}
