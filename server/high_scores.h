/*
High scores! implemented with a cool binary tree
*/

#ifndef HIGH_SCORES_H
#define HIGH_SCORES_H

#define MAX_USERNAME_SIZE 32

/* High scores - binary tree */
struct Record {
	int user_id;
	int seconds;
	struct Record* lchild;
	struct Record* rchild;
};

struct Record* ROOT;

void add_record(int user_id, int finish_time);
// void reverse_order(struct Node* root, (void*)(action));

/* Win statistics - ordinary array */
struct WinStatistic {
	int games_won;
	int games_played;
};

struct WinStatistic* statistics;

void init_statistics(int len);
void free_statistics();
void free_records(struct Record* root);
void on_win(int user_id, int finish_time);
void on_loss(int user_id);


#endif