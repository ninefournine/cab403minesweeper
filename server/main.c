#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <signal.h>
#include <pthread.h>
#include <time.h>

#include "sockets.h"
#include "threadpool.h"
#include "auth.h"
#include "minesweeper.h"
#include "../common/packets.h"
#include "../common/constants.h"
#include "high_scores.h"
#include "readerwriter.h"

#define DEFAULT_PORT "12345"
#define RANDOM_NUMBER_SEED 42

int get_authentication(int fd, char* username) {
	char password[MAX_PASSWORD_LENGTH];
	// Get the data from the client.
	if (packet_recv(fd, "ss", username, password) == -1) {
		return 0;
	}
	return authenticate(username, password);
}

int start_game(int fd, struct GameState* game) {
	printf("Starting new game...");
	gamestate_init(game);
	// Send the client the initial number of mines.
	packet_send(fd, "c", game->remaining_mines);
	return 1;
}

int reveal_tiles(int fd, struct GameState* game, int user_id) {
	printf("Revealing tiles...");

	// Get the coordinates from the client.
	uint8_t row, column;
	if (packet_recv(fd, "cc", &row, &column) == -1) {
		return 0;
	}
	printf("row: %d, column: %d\n", row, column);

	struct Response response;
	// number of mines won't change here.
	response.number_of_mines = game->remaining_mines;

	// Let's make our response.
	reveal_tile(game, (int)row, (int)column, &response);

	// send the response object.
	packet_send(fd, "cc#c#c#c",
		response.response_code,
		response.number_of_mines,
		response.number_of_tiles,
		response.x_coordinates,
		response.number_of_tiles,
		response.y_coordinates,
		response.number_of_tiles,
		response.characters);

	if (response.response_code == RESP_GAME_OVER) {
		on_loss(user_id);
	}

	return 1;
}

int flag_place_server(int fd, struct GameState* game, int user_id) {
	//Get the coordinates from the client.
	uint8_t row, column;
	if (packet_recv(fd, "cc", &row, &column) == -1) {
		return 0;
	}
	printf("row: %d, column: %d\n", row, column);

	uint8_t response_code;
	place_flag(game, (int)row, (int)column, &response_code);

	packet_send(fd, "cc", response_code,game->remaining_mines);
	if (game->remaining_mines == 0){ // victory!
		// Calculate time to finish.
		time_t finish;
		time(&finish);
		uint16_t time_taken = (uint16_t)difftime(finish,game->begin);
		printf("Time Taken: %d\n", time_taken);

		// Let the client know the whole board.
		char data[TOTAL_TILES];
		int count = 0;
		for (int j = 0; j<NUM_TILES_Y; j++){
			for (int i = 0; i<NUM_TILES_X; i++) {
				if (game->tiles[j+1][i+1].flag_placed){
					data[count] = 'P';
				}
				else {
					data[count] = (char)game->tiles[j+1][i+1].adjacent_mines + '0';
				}
				count++;
			}
		}
		packet_send(fd,"#ch",TOTAL_TILES, data, time_taken);

		// Update the scoreboard.
		on_win(user_id, time_taken);
	}
	return 1;
}

void reverse_order(int fd, struct Record* root, struct Record* smallest) {
	if (root == NULL) {
		return;
	} else {
		reverse_order(fd, root->rchild, smallest);
		int keep_going = 1;
		if (root == smallest) {
			keep_going = 0;
		}
		packet_send(fd, "cshhh",
			keep_going,	// Is this the last item?
			get_username_from_id(root->user_id),
			root->seconds,
			statistics[root->user_id].games_won,
			statistics[root->user_id].games_played);
		reverse_order(fd, root->lchild, smallest);
	}
}

int show_leaderboard(int fd) {
	//Debugging code
	// printf("entered here!\n");
	uint8_t have_records;
	// Get the smallest item.
	ReadLock();
	struct Record* smallest = ROOT;
	while (smallest != NULL && smallest->lchild != NULL) {
		smallest = smallest->lchild;
	}
	if (smallest == NULL) {
		have_records = 0;
		packet_send(fd, "c", have_records);
	} else {
		have_records = 1;
		packet_send(fd, "c", have_records);
		reverse_order(fd, ROOT, smallest);
	}
	ReadUnlock();
	return 1;
}


void handle_request(struct request* a_request, int thread_id) {

	int fd = a_request->fd;
	struct GameState game;

	char username[32];
	// authenticating
	int user_id = get_authentication(fd, username);
	uint8_t response = user_id != -1 ? 1 : 0;
	// tell the client if they're authenticated or not.
	if (packet_send(fd, "c", response) == -1) {
		user_id = -1;	// if they've disconnected, undo the authentication
	}
	// Enter the main loop after authentication.
	int still_going = 1;
	if (user_id != -1) {
		while (still_going) {
			char client_code;
			int req_result;
			// Get the code from the client.
			if (packet_recv(fd, "c", &client_code) == -1) {
				// Client disconnected.
				still_going = 0;
				break;
			}
			switch(client_code) {
				case '1': // start a new game
					req_result = start_game(fd, &game);
					if (req_result == 0) {
						still_going = 0;
					}
					break;
				case '2': //show leaderboard
					req_result = show_leaderboard(fd);
					if (req_result == 0) {
						still_going = 0;
					}
					break;
				case 'R':
				case 'r':
					req_result = reveal_tiles(fd, &game, user_id);
					if (req_result == 0) {
						still_going = 0;
					}
					break;
				case 'P':
				case 'p':
					req_result = flag_place_server(fd, &game, user_id);
					if (req_result == 0) {
						still_going = 0;
					}
					break;
				default:
					printf("Invalid code...\n");
					break;
			}
		}
	}
	close(fd);
}

void handle_signal(int signal) {
    // assume it's SIGINT for now.
    printf("Caught SIGINT, exiting now\n");

	// Free the memory allocated for authentication.
	free_authentication_data();
	free_statistics();
	free_records();
    exit(0);
}

void set_signal_handler(void* handler) {
    struct sigaction sa;
    sa.sa_handler = handler;
    sa.sa_flags = SA_RESTART;
    sigfillset(&sa.sa_mask);
    if (sigaction(SIGINT, &sa, NULL) == -1) {
        perror("Error: cannot handle SIGINT"); // Should not happen
    }
}


int main(int argc, char* argv[])
{
    // Ignore SIGPIPE.
    signal(SIGPIPE, SIG_IGN);
    // request ID counter. each request has a unique ID (even ones that were terminated)
    int request_counter = 0;

	char* port;
    // Check argument count is correct.
	if (argc < 2) {
		fprintf(stderr, "You did not specify a port. Using default port %s.\n", DEFAULT_PORT);
		port = DEFAULT_PORT;
	} else {
		port = argv[1];
	}

	srand(RANDOM_NUMBER_SEED);
	int count = load_authentication_data("Authentication.txt");
	init_statistics(count);
    set_signal_handler(&handle_signal);

	ReadWriteMutexInit( );

	int sockfd = setup_sockets(port);

    request_handler = &handle_request;
    init_threadpool();

    printf("server: waiting for connections...\n");

    int new_fd;  // new connection on new_fd
    struct sockaddr_storage their_addr; // connector's address information
    socklen_t sin_size;

    while(1) {  // main accept() loop
        sin_size = sizeof their_addr;
		// Main thread blocks here...
        new_fd = wait_for_new_connection(sockfd, (struct sockaddr *)&their_addr, &sin_size);
        add_request(request_counter, new_fd);
        request_counter++;
    }
	pthread_exit(NULL);


    return 0;
}
