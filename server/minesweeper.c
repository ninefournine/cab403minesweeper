#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>

#include "minesweeper.h"
#include "trand.h"
#include "../common/constants.h"

/*
Creates a new gamestate using malloc.
(We ended up not using this.)
*/
struct GameState* make_new_gamestate() {
	struct GameState* new_state = malloc(sizeof(struct GameState));
	if (new_state == NULL) { // malloc failed!?
		perror("malloc");
		exit(1);
	}
	gamestate_init(new_state);

	return new_state;
}

/*
Clears the board of a gamestate of all mines.
*/
void clear_board(struct GameState* game) {
	for (int x = 0; x < NUM_TILES_X+2; x++) {
		for (int y = 0; y < NUM_TILES_Y+2; y++) {
			game->tiles[y][x].is_mine = false;
			game->tiles[y][x].flag_placed = false;
		}
	}
}

/*
Initialises a gamestate with mines.
*/
void gamestate_init(struct GameState* game) {
	clear_board(game);
	game->remaining_mines = NUM_MINES;
	place_mines(game);
	count_adjacent_mines(game);
	game->begin = time(NULL);
	game->game_over = false;
	game->win = false;
	time(&game->begin);
}

/*
Place mines according to algorithm in notes.
*/
void place_mines(struct GameState* game){
  for (int i = 0; i < NUM_MINES; i++)
  {
    int x, y;
    do{
      x = (trand() % NUM_TILES_X) + 1;
      y = (trand() % NUM_TILES_Y) + 1;
  } while(tile_contains_mine(game, y,x));
    game->tiles[y][x].is_mine = true;
  }
}

/*
Returns if a tile contains a mine or not.
*/
bool tile_contains_mine(struct GameState* game, int y, int x){
  return game->tiles[y][x].is_mine;
}

/*
After placing the mines, pre-populates each tile with the adjacent mines.
*/
void count_adjacent_mines(struct GameState* game){
  for (int y = 1; y <= NUM_TILES_Y; y++){
    for (int x = 1; x <= NUM_TILES_X; x++){
      int mine_counter = 0;
      for (int i = -1; i <= 1; i++){
        for (int j = -1; j <= 1; j++){
          if (tile_contains_mine(game,y+i,x+j)){
            mine_counter ++;
          }
        }
        game->tiles[y][x].adjacent_mines = mine_counter;
        game->tiles[y][x].is_revealed = false;
      }
    }
  }
}

/*
Reveals a tile. The response is stored in the passed Response object.
*/
void reveal_tile(struct GameState* game, int y, int x, struct Response* response){
	if (y < 1 || y > NUM_TILES_Y || x < 1 || x > NUM_TILES_X) {
	  // they're out of bounds.
	  response->response_code = RESP_OUT_OF_BOUNDS;
	  response->number_of_tiles = 0;
	  return;
	}
  if (!game->tiles[y][x].is_revealed){
    if (tile_contains_mine(game,y,x)){ // they hit a mine!
      game->game_over = true;
      game->win = false;
	  response->response_code = RESP_GAME_OVER; 	// game over
	  // Iterate through the board and add the mines...
	  response->number_of_tiles = 0;
	  for (int cy = 1; cy <= NUM_TILES_Y; cy++) {
		for (int cx = 1; cx <= NUM_TILES_X; cx++) {
			if (tile_contains_mine(game, cy, cx)) {
				response->x_coordinates[response->number_of_tiles] = cx;
				response->y_coordinates[response->number_of_tiles] = cy;
				response->characters[response->number_of_tiles] = '*';
				response->number_of_tiles++;
			}
		}
	  }
      return;
    }
    else { // they hit a normal tile!
      game->tiles[y][x].is_revealed = true;
	  response->response_code = RESP_ALL_GOOD;	// good
	  response->number_of_tiles = 1;
	  response->x_coordinates[0] = x;
	  response->y_coordinates[0] = y;
	  response->characters[0] = (char)(game->tiles[y][x].adjacent_mines) + '0';

      if (game->tiles[y][x].adjacent_mines > 0){
        return;
      }
      else {
        flood_fill_reveal(game,y,x,response);
        return;
      }
    }
  }
  else{ // they hit an already revealed tile.
	  response->response_code = RESP_ALREADY_REVEALED;	// already revealed
	  response->number_of_tiles = 0;
	  return;
  }
}


void flood_fill_reveal(struct GameState* game, int y, int x, struct Response* response){

  for (int i = -1; i <= 1; i++){
    if (y+i <= NUM_TILES_Y && y+i > 0){
      for (int j = -1; j<= 1; j++){
        if (game->tiles[y+i][x+j].is_revealed){
          //Skip checking if already revealed.
        }
        else{
          if (x+j <= NUM_TILES_X && x+j > 0){
            if (!game->tiles[y+i][x+j].is_mine) {
              game->tiles[y+i][x+j].is_revealed = true;
			  response->x_coordinates[response->number_of_tiles] = x+j;
			  response->y_coordinates[response->number_of_tiles] = y+i;
			  response->characters[response->number_of_tiles] = (char)(game->tiles[y+i][x+j].adjacent_mines) + '0';
			  response->number_of_tiles++;
              if (game->tiles[y+i][x+j].adjacent_mines == 0){
                flood_fill_reveal(game, y+i, x+j, response);
              }
            }
          }
        }
      }
    }
  }
}

/*
Places a flag. In this case we don't use an entire Response object since there
is a lot of fields not needed for just a flag.
*/
void place_flag(struct GameState* game, int y, int x, uint8_t* response_code){
  if (y < 1 || y > NUM_TILES_Y || x < 1 || x > NUM_TILES_X) {
	*response_code = RESP_OUT_OF_BOUNDS;
	return;
  }
  if (!game->tiles[y][x].is_revealed){
    if (tile_contains_mine(game, y,x)){
      game->tiles[y][x].is_revealed = true;
      game->tiles[y][x].flag_placed = true;
      game->remaining_mines--;
      if (game->remaining_mines == 0){
        game->game_over = true;
        game->win = true;
      }
  } else {
	  return;
  }
  }
  else{
    *response_code = RESP_ALREADY_REVEALED;
    return;
  }
}
