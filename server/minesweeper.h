#ifndef MINESWEEPER_H
#define MINESWEEPER_H

#include <stdbool.h>
#include <time.h>
#include <stdint.h>
#include "../common/constants.h"
#include "../common/packets.h"



#define RANDOM_NUMBER_SEED 42
#define NUM_MINES 1

typedef struct
{
  int adjacent_mines;
  bool is_revealed;
  bool is_mine;
  bool flag_placed;
} Tile;

struct GameState
{
  int remaining_mines;
  int remaining_flags;
  Tile tiles[NUM_TILES_Y+2][NUM_TILES_X+2];
  time_t begin;
  bool game_over;
  bool win;
};



struct GameState* make_new_gamestate(void);
void gamestate_init(struct GameState* game);
void place_mines(struct GameState* game);
bool tile_contains_mine(struct GameState* game, int y, int x);
void count_adjacent_mines(struct GameState* game);
void reveal_tile(struct GameState* game, int y, int x, struct Response* response);
void flood_fill_reveal(struct GameState* game, int y, int x, struct Response* response);
void place_flag(struct GameState* game, int y, int x, uint8_t* response_code);

#endif
