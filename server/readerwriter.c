/*
Readers/Writers problem methods. Taken directly from prac 5
*/

#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include "readerwriter.h"

int 			ready = 0;
int 			go = 0;
pthread_mutex_t ready_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t 	ready_cond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t go_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t 	go_cond = PTHREAD_COND_INITIALIZER;

// Don't need screen mutex because each thread writes to a different screen.
// pthread_mutex_t screen = PTHREAD_MUTEX_INITIALIZER;

int rc;	/* readcount */
pthread_mutex_t rc_mutex;
pthread_mutex_t r_mutex;
pthread_mutex_t w_mutex;

/* Init of the mutexes */

void ReadWriteMutexInit( ) {

	rc = 0;
	pthread_mutex_init( &rc_mutex, NULL );
	pthread_mutex_init( &r_mutex, NULL );
	pthread_mutex_init( &w_mutex, NULL );
	}

/* Read action lock */

void ReadLock( ) {

	pthread_mutex_lock( &r_mutex );
	pthread_mutex_lock( &rc_mutex );
	rc++;
	if ( rc == 1 ) pthread_mutex_lock( &w_mutex );
	pthread_mutex_unlock( &rc_mutex );
	pthread_mutex_unlock( &r_mutex );
	}

/* Read action unlock */

void ReadUnlock( ) {

	pthread_mutex_lock( &rc_mutex );
	rc--;
	if (rc == 0) pthread_mutex_unlock( &w_mutex );
	pthread_mutex_unlock( &rc_mutex );
	}

/* Write action lock */

void WriteLock( ) {

	pthread_mutex_lock(&r_mutex);
	pthread_mutex_lock(&w_mutex);
	}

/* Write action unlock */

void WriteUnlock( ) {
	pthread_mutex_unlock(&w_mutex);
	pthread_mutex_unlock(&r_mutex);
	}
	
void ReadyToGo( )	{

	pthread_mutex_lock( &ready_mutex );
	ready++;
	pthread_mutex_unlock( &ready_mutex );
	pthread_cond_signal( &ready_cond );
	pthread_mutex_lock( &go_mutex );
	while ( go == 0 ) pthread_cond_wait( &go_cond, &go_mutex );
	pthread_mutex_unlock( &go_mutex );
	}
