#ifndef READERWRITER_H
#define READERWRITER_H

#define READERS 4
#define WRITERS 4

void ReadWriteMutexInit( );
void ReadLock( );
void ReadUnlock( );
void WriteLock( );
void WriteUnlock( );
void ReadyToGo( );

#endif