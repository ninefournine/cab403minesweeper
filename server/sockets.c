#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>

#include "sockets.h"


// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

/*
Returns a socket file descriptor, given the port to listen to.
The port is a string e.g. "4960"
*/
int setup_sockets(char* port) {
	int sockfd = 0;  // file descriptor for our listening socket
	struct addrinfo hints;
	struct addrinfo* servinfo;
	struct addrinfo* p;	// pointer to the addrinfo that we binded to
	
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC; // IPv4 or IPv6 okay
	hints.ai_socktype = SOCK_STREAM; // TCP sockets
	hints.ai_flags = AI_PASSIVE; // use my IP
	
	int rv;
	if ((rv = getaddrinfo(NULL, port, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}
	
	int yes=1;
	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		// Make socket.
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("server: socket");
			continue;
		}

		// Set SO_REUSEADDR
		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
				sizeof(int)) == -1) {
			perror("setsockopt");
			exit(1);
		}

		// Try to bind.
		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("server: bind");
			continue;
		}

		// If we didn't hit any of the continues then we reach here and p is good.
		break;
	}
	
	if (p == NULL)  {
		fprintf(stderr, "server: failed to bind\n");
		exit(1);
	}

	if (listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(1);
	}

	return sockfd;
}

/*
Blocks while it waits for a new connection. Returns the file descriptor of the
new connection.
*/
int wait_for_new_connection(int sockfd, struct sockaddr* addr, socklen_t* addrlen) {
	int new_fd = accept(sockfd, addr, addrlen);
    char s[INET6_ADDRSTRLEN];
	if (new_fd == -1) {
		perror("accept");
	}
	
	inet_ntop(addr->sa_family,
		get_in_addr(addr),
		s, sizeof s);
	printf("server: got connection from %s\n", s);
	return new_fd;
}
