/*
Socket communication. Based primarily on the examples from Beej's Guide to Network
Programming:
https://beej.us/guide/bgnet/
*/


#ifndef SOCKETS_H
#define SOCKETS_H

#define BACKLOG 10     // how many pending connections queue will hold

// get sockaddr, IPv4 or IPv6.
void *get_in_addr(struct sockaddr *sa);

/*
Returns a socket file descriptor, given the port to listen to.
The port is a string e.g. "4960"
*/
int setup_sockets(char* port);

/*
Blocks while it waits for a new connection. Returns the file descriptor of the
new connection.
*/
int wait_for_new_connection(int sockfd, struct sockaddr* addr, socklen_t* addrlen);

#endif