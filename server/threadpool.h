/*
Thread pool library. Based primarily on prac 4 examples.
*/

#ifndef THREADPOOL_H
#define THREADPOOL_H

/* how many threads? */
#define NUM_THREADS 10

/* format of a single request. */
struct request {
    int number;             /* number of the request                  */
    int fd;                 /* socket to talk to.                     */
    struct request* next;   /* pointer to next request, NULL if none. */
};

pthread_mutex_t request_mutex;
pthread_cond_t  got_request;
void (*request_handler)(struct request*, int);

void add_request(int request_num,
            int new_fd);

struct request* get_request(pthread_mutex_t* p_mutex);

void* request_loop(void* data);
void init_threadpool();



#endif
