#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <pthread.h>

#include "trand.h"

pthread_mutex_t random_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;

int trand() {
	int result;
	pthread_mutex_lock(&random_mutex);
	result = rand();
	pthread_mutex_unlock(&random_mutex);
	return result;
}