/* trand.h - thread safe random. */

#ifndef TRAND_H
#define TRAND_H

int trand();

#endif